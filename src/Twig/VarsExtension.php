<?php

declare(strict_types=1);

namespace WebWMS\Twig;

use JsonException;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * @package:    WebWMS\Twig
 * @author:     SoftDev Nord, Rene Irrgang
 * @copyright:  Copyright © 2019-2023, SoftDev Nord
 * Class        VarsExtension
 */
class VarsExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('json_decode', $this->jsonDecode(...)),
        ];
    }

    /**
     * @throws JsonException
     */
    public function jsonDecode(string $str): mixed
    {
        return json_decode($str, false, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName(): string
    {
        return 'vars_extension';
    }
}
