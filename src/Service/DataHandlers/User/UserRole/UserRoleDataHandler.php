<?php

declare(strict_types=1);

namespace WebWMS\Service\DataHandlers\User\UserRole;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use WebWMS\Entity\UserRoleEntity;
use WebWMS\Service\DateTimeService;

/**
 * @package:    WebWMS\Service\DataHandlers\Role
 * @author:     SoftDev Nord, Rene Irrgang
 * @copyright:  Copyright © 2019-2023, SoftDev Nord
 * Class        UserRoleDataHandler
 */
class UserRoleDataHandler
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly DateTimeService $dateTimeService
    ) {
    }

    public function save(UserRoleEntity $userRoleEntity): void
    {
        $this->entityManager->persist($userRoleEntity);
        $this->entityManager->flush();
    }

    public function delete(UserRoleEntity $userRoleEntity): void
    {
        $this->entityManager->remove($userRoleEntity);
        $this->entityManager->flush();
    }

    /**
     * @return array<int, UserRoleEntity>
     */
    public function getAllUserRoles(): array
    {
        return $this->entityManager
            ->getRepository(UserRoleEntity::class)
            ->findAll();
    }

    public function getUserRoleById(int $userRoleId): ?UserRoleEntity
    {
        return $this->entityManager
            ->getRepository(UserRoleEntity::class)
            ->findOneBy(['id' => $userRoleId]);
    }

    public function getUserRoleByUserRoleName(string $userRoleName): ?UserRoleEntity
    {
        return $this->entityManager
            ->getRepository(UserRoleEntity::class)
            ->findOneBy(['user_role' => $userRoleName]);
    }

    public function addUserRole(Request $request): ?UserRoleEntity
    {
        $addUserRole = $request->request->getIterator()->getArrayCopy();
        $userRoleEntity = new UserRoleEntity();

        $userRoleEntity->setUserRole($addUserRole['user_role']);
        $userRoleEntity->setDescription($addUserRole['description']);
        $userRoleEntity->setCreatedAt($this->dateTimeService->createDateTime());

        $this->save($userRoleEntity);

        return $userRoleEntity;
    }

    public function updateUserRole(Request $request): ?UserRoleEntity
    {
        $requestData = $request->request->all()['edit_user_role'];
        $userRole = $this->entityManager
            ->getRepository(UserRoleEntity::class)
            ->findOneBy(['user_role' => $requestData['user_role']]);

        if ($userRole === null) {
            return null;
        }

        $userRole->setUserRole($requestData['user_role']);
        $userRole->setDescription($requestData['description']);
        $userRole->setUpdatedAt($this->dateTimeService->createDateTime());

        $this->save($userRole);

        return $userRole;
    }

    public function deleteUserRole(string $userRoleName): void
    {
        $userRole = $this->getUserRoleByUserRoleName($userRoleName);

        if ($userRole instanceof UserRoleEntity) {
            $this->delete($userRole);
        }
    }
}
