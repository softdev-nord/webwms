<?php

declare(strict_types=1);

namespace WebWMS\Service\Validation;

/**
 * @package:    WebWMS\Service\Validation
 * @author:     SoftDev Nord, Rene Irrgang
 * @copyright:  Copyright © 2019-2023, SoftDev Nord
 * Class        SupplierValidationService
 */
class SupplierValidationService
{
    /**
     * @param array<mixed> $requestData
     * @return array<string, array<string, string>|bool|int|string>
     *
     * @SuppressWarnings(PHPMD.ElseExpression)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function validateSupplierData(array $requestData): array
    {
        $responseData = [];

        if (!$requestData['supplierNr']) {
            $responseData['error']['supplierNr'] = 'Die Lieferanten-Nr. darf nicht leer sein.';
        } else {
            $responseData['supplierNr'] = $requestData['supplierNr'];
        }

        if (!$requestData['supplierName']) {
            $responseData['error']['supplierName'] = 'Der Lieferanten-Name darf nicht leer sein.';
        } else {
            $responseData['supplierName'] = $requestData['supplierName'];
        }

        if (!$requestData['supplierAddressStreet']) {
            $responseData['error']['supplierAddressStreet'] = 'Die Straße darf nicht leer sein.';
        } else {
            $responseData['supplierAddressStreet'] = $requestData['supplierAddressStreet'];
        }

        if (!$requestData['supplierAddressStreetNr']) {
            $responseData['error']['supplierAddressStreetNr'] = 'Die Hausnummer darf nicht leer sein.';
        } else {
            $responseData['supplierAddressStreetNr'] = $requestData['supplierAddressStreetNr'];
        }

        if (!$requestData['supplierAddressCountryCode']) {
            $responseData['error']['supplierAddressCountryCode'] = 'Das Land darf nicht leer sein.';
        } else {
            $responseData['supplierAddressCountryCode'] = $requestData['supplierAddressCountryCode'];
        }

        if (!$requestData['supplierAddressZipcode']) {
            $responseData['error']['supplierAddressZipcode'] = 'Die Postleitzahl darf nicht leer sein.';
        } else {
            $responseData['supplierAddressZipcode'] = $requestData['supplierAddressZipcode'];
        }

        if (!$requestData['supplierAddressCity']) {
            $responseData['error']['supplierAddressCity'] = 'Die Stadt darf nicht leer sein.';
        } else {
            $responseData['supplierAddressCity'] = $requestData['supplierAddressCity'];
        }

        if (!isset($responseData['error'])) {
            $responseData['success'] = true;
        }

        return $responseData;
    }
}
