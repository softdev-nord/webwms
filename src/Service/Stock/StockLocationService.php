<?php

declare(strict_types=1);

namespace WebWMS\Service\Stock;

use Doctrine\DBAL\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use WebWMS\Entity\StockLocationEntity;
use WebWMS\Service\DataHandlers\Stock\StockLocationDataHandler;

/**
 * @package:    WebWMS\Service\Stock
 * @author:     SoftDev Nord, Rene Irrgang
 * @copyright:  Copyright © 2019-2023, SoftDev Nord
 * Class        StockLocationService
 */
class StockLocationService
{
    public function __construct(
        private readonly StockLocationDataHandler $stockLocationDataHandler
    ) {
    }

    /**
     * @throws Exception
     */
    public function getAllStockLocations(): JsonResponse
    {
        return $this->stockLocationDataHandler->getAllStockLocation();
    }

    public function getStockLocationByCoordinate(string $stockLocationCoordinate): ?StockLocationEntity
    {
        return $this->stockLocationDataHandler
            ->getStockLocationByCoordinate(
                $stockLocationCoordinate
            );
    }

    public function getStockLocationById(int $stockLocationId): ?StockLocationEntity
    {
        return $this->stockLocationDataHandler->getStockLocationById($stockLocationId);
    }

    /**
     * @throws \Exception
     * @return object[]
     */
    public function getStockLocationDetailsById(string $stockLocationId): array
    {
        return $this->stockLocationDataHandler->getStockLocationDetailsById($stockLocationId);
    }

    public function addStockLocation(StockLocationEntity $stockLocationEntity): void
    {
        $this->stockLocationDataHandler->addStockLocation($stockLocationEntity);
    }

    public function updateStockLocation(StockLocationEntity $stockLocationEntity): void
    {
        $this->stockLocationDataHandler->updateStockLocation($stockLocationEntity);
    }

    public function deleteStockLocation(StockLocationEntity $stockLocationEntity): void
    {
        $this->stockLocationDataHandler->deleteStockLocation($stockLocationEntity);
    }

    /**
     * @throws Exception
     * @return array<string|int|mixed>
     */
    public function getAllStockLocationsForSelect(): array
    {
        return $this->stockLocationDataHandler->getAllStockLocationsForSelect();
    }

    /**
     * @throws \Exception
     * @return object[]
     */
    public function getAllStockLocationsAjax(): array
    {
        return $this->stockLocationDataHandler->getAllStockLocationsAjax();
    }

    /**
     * @throws Exception
     * @return array<string|int|mixed>
     */
    public function getAllFreeStockLocations(string $stockSystem): array
    {
        return $this->stockLocationDataHandler->getAllFreeStockLocations($stockSystem);
    }

    /**
     * @throws Exception
     * @return array<string|int|mixed>
     */
    public function getAllFreeStockLocationsWithLimit(string $stockSystem, int $limit, float $leQuantity): array
    {
        return $this->stockLocationDataHandler
            ->getAllFreeStockLocationsWithLimit(
                $stockSystem,
                $limit,
                $leQuantity
            );
    }

    /**
     * @throws Exception
     * @return array<int, mixed>
     */
    public function getFirstFreeStockLocation(string $stockSystem, int $limit, float $leQuantity): array
    {
        $freeStockLocation = [];
        $stockLocations = $this->getAllFreeStockLocationsWithLimit($stockSystem, $limit, $leQuantity);

        foreach ($stockLocations as $stockLocation) {
            if ($stockLocation['belegt'] !== true) {
                $freeStockLocation[] = $stockLocation;
            }
        }

        return $freeStockLocation;
    }

    /**
     * @param  array<string> $stockLocation
     * @return array<int, array<string, int|string>>
     */
    public function getRemainder(array $stockLocation, ?int $remainder): array
    {
        $freeStockLocations = [];

        if (isset($remainder)) {
            $freeStockLocations[] = [
                'ln' => $stockLocation['ln'],
                'fb' => $stockLocation['fb'],
                'sp' => $stockLocation['sp'],
                'tf' => $stockLocation['tf'],
                'lnKomplett' => $stockLocation['ln'] . '-' . $stockLocation['fb'] . '-' . $stockLocation['sp'] . '-' . $stockLocation['tf'],
                'koordinate' => $stockLocation['koordinate'],
                'system' => $stockLocation['system'],
                'quantity' => $remainder,
            ];
        }

        return $freeStockLocations;
    }

    /**
     * @throws Exception
     * @return array<int, array<string, int|string>>
     */
    public function getOccupiedStockLocationsByArticleId(int $articleId): array
    {
        return $this->stockLocationDataHandler->getOccupiedStockLocationsByArticleId($articleId);
    }
}
