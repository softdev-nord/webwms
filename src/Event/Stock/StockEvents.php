<?php

declare(strict_types=1);

namespace WebWMS\Event\Stock;

/**
 * @package:    WebWMS\Event\Stock
 * @author:     SoftDev Nord, Rene Irrgang
 * @copyright:  Copyright © 2019-2024, SoftDev Nord
 * Class        StockEvents
 */
class StockEvents
{
    public const KARTON = 'Durchlaufregal';

    public const PALETTE = 'Pal Regal';

    public const BLOCK = 'Block-Lager';
}
