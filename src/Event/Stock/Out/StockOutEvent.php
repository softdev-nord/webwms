<?php

declare(strict_types=1);

namespace WebWMS\Event\Stock\Out;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * @package:    WebWMS\Event\Stock\Out
 * @author:     SoftDev Nord, Rene Irrgang
 * @copyright:  Copyright © 2019-2024, SoftDev Nord
 * Class        StockOutEvent
 */
class StockOutEvent extends Event
{
    final public const EVENT_NAME = 'stock.stock_out';

    final public const EVENT = 'SO101';

    /**
     * SO101 Auslagern direkt
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function stockOut(Request $request): RedirectResponse|Response
    {
        // TODO: Implement logic
        return new Response('Example Response');
    }
}
