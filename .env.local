# In all environments, the following files are loaded if they exist,
# the latter taking precedence over the former:
#
#  * .env                contains default values for the environment variables needed by the app
#  * .env.local          uncommitted file with local overrides
#  * .env.$APP_ENV       committed environment-specific defaults
#  * .env.$APP_ENV.local uncommitted environment-specific overrides
#
# Real environment variables win over .env files.
#
# DO NOT DEFINE PRODUCTION SECRETS IN THIS FILE NOR IN ANY OTHER COMMITTED FILES.
#
# Run "composer dump-env prod" to compile .env files for production use (requires symfony/flex >=1.2).
# https://symfony.com/doc/current/best_practices.html#use-environment-variables-for-infrastructure-configuration

LOCALE=de

# this is used for loading uploaded files embedded in a PDF Template, the host of the web server must be set
# in a docker environment the internal host name of the web container must be specified, e.g. "http://web"
# otherwise the host name of your web server must be set, e.g. https://pve
WEB_HOST=http://web

###> composer/composer ###
COMPOSER_HOME=/var/www/html/var/cache/composer
###< composer/composer ###

###> symfony/framework-bundle ###
APP_ENV=dev
APP_SECRET=af2b45ff237087d068938bb0858bddff
APP_DEBUG=true
###< symfony/framework-bundle ###

###> webWMS ###
APP_NAME=' | webWMS Das webbasierte Lagerverwaltungssystem'
APP_VERSION='Enterprise Version'
APP_VERSION_NUMBER='2.0.0'
APP_COPYRIGHT='© 2019-2023 Softdev-Nord | Rene Irrgang'
APP_LIZENZ='Demo Spedition | Demoweg 500 | 21698 Harsefeld'
###< webWMS ###

###> doctrine/doctrine-bundle ###
DATABASE_URL=mysql://root:sdndocker@database:3306/webWMS?serverVersion=10.9.3-MariaDB
###< doctrine/doctrine-bundle ###

### mailer settings ###
FROM_MAIL=info@softdev-nord.de
FROM_NAME='webWMS Test'
RETURN_PATH=info@softdev-nord.de
# sent copy of mail to the address specified in FROM_MAIL
MAIL_COPY=true
MAIL_MAILER=smtp
MAIL_HOST=127.0.0.1
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
### mailer settings ###

###> Docker webWMS ###

############################################################################
# Compose project name
# To determine the name of your containers
############################################################################
COMPOSE_PROJECT_NAME=webwms
############################################################################
# Select PHP version
# Possible values: php8.2, php8.3
############################################################################
PHPVERSION=php8.3
# If you need APCu Cache, activate it here
PHP_INSTALL_APCU=true
# If you need ImageMagick, activate it here
PHP_INSTALL_IMAGEMAGICK=true
# If you need IonCube Loader, activate it here
PHP_INSTALL_IONCUBE=true # PHP 8.0 is not supported yet. Reference: https://forum.ioncube.com/viewtopic.php?t=4592
# If you need Zend OPCache, activate it here
PHP_INSTALL_OPCACHE=true
# If you need XDebug, activate it here
PHP_INSTALL_XDEBUG=true
# If you need Redis, activate it here
PHP_INSTALL_REDIS=true
PHP_INI=./.docker/config/php/php.ini
OPCACHE_INI=./.docker/config/php/opcache.ini
XDEBUG_INI=./.docker/config/php/xdebug.ini
############################################################################
# Select SQL server
# Possible values: MariaDB10.5, MySQL5.7, MySQL8.0
############################################################################
DATABASE=MariaDB10.5
MYSQL_DATA_DIR=./.docker/data/mysql
MYSQL_LOG_DIR=./.docker/logs/mysql
############################################################################
# Apache settings
############################################################################
# If you already have the port 80 in use, you can change it
HOST_MACHINE_UNSECURE_HOST_PORT=8080

# If you already have the port 443 in use, you can change it
HOST_MACHINE_SECURE_HOST_PORT=8445

DOCUMENT_ROOT=./
APACHE_DOCUMENT_ROOT=/var/www/html
VHOSTS_DIR=./.docker/config/vhosts/apache
APACHE_LOG_DIR=./.docker/logs/apache2
APACHE_CERTS_DIR=./.docker/config/certs

# Enable apache modules
APACHE_ENABLE_MOD_REWRITE=true
APACHE_ENABLE_MOD_HEADERS=true
APACHE_ENABLE_MOD_SSL=true

# User Id and group
USER_ID=1000
GROUP_ID=1000
############################################################################
# Database settings
#
# If you need to give the docker user access to more databases than the
#"docker" db you can grant the privileges with phpmyadmin to the user.
############################################################################
# If you already have the port 3306 in use, you can change it
HOST_MACHINE_MYSQL_PORT=3310
MYSQL_ROOT_PASSWORD=sdndocker
MYSQL_USER=sdndocker
MYSQL_PASSWORD=sdndocker
MYSQL_DATABASE=webWMS
MYSQL_ENTRYPOINT_INITDB=./.docker/sql/docker-entrypoint-initdb.d
############################################################################
# Redis settings
############################################################################
# If you already has the port 6379 in use, you can change it
HOST_MACHINE_REDIS_PORT=6679
############################################################################
# PhpMyAdmin settings
############################################################################
# If you already have the port 8080 in use, you can change it
HOST_MACHINE_PMA_PORT=8083
HOST_MACHINE_SECURE_PMA_PORT=8155
############################################################################
# MailHog settings
############################################################################
# If you already has the port 8025 in use, you can change it
HOST_MACHINE_MAILHOG_HTTP_PORT=8135
# If you already has the port 1025 in use, you can change it
HOST_MACHINE_MAILHOG_SMTP_PORT=1135
############################################################################
# Database backup
############################################################################
MYSQL_BACKUP_DIR=./.docker/sql/backup/mysql
BACKUP_CRONTAB=./.docker/php7.4/cron.d/backup-db
BACKUP_FILE=./.docker/sql/backup/mysql/backup-database.sh
############################################################################
###< Docker webWMS ###

###> symfony/mailer ###
# MAILER_DSN=null://null
###< symfony/mailer ###