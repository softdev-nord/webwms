<?php

declare(strict_types=1);

namespace WebWMS\Tests\Unit\Service\Stock;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use WebWMS\Entity\StockLayoutEntity;
use WebWMS\Service\DataHandlers\Stock\StockLayoutDataHandler;
use WebWMS\Service\Stock\StockLayoutService;

/**
 * @package:    WebWMS\Tests\Unit\Service\Stock
 * @author:     SoftDev Nord, Rene Irrgang
 * @copyright:  Copyright © 2019-2024, SoftDev Nord
 * Class        StockLayoutServiceTest
 */
#[CoversClass(StockLayoutService::class)]
final class StockLayoutServiceTest extends TestCase
{
    private StockLayoutService $stockLayoutService;

    private MockObject $mockObject;

    protected function setUp(): void
    {
        $this->mockObject = $this->createMock(StockLayoutDataHandler::class);
        $this->stockLayoutService = new StockLayoutService($this->mockObject);
    }

    public function testGetAllStockLayouts(): void
    {
        $stockLayouts = ['layout1', 'layout2'];

        $this->mockObject
            ->expects(self::once())
            ->method('getAllStockLayouts')
            ->willReturn(new JsonResponse($stockLayouts));

        $result = $this->stockLayoutService->getAllStockLayouts();

        self::assertInstanceOf(JsonResponse::class, $result);
    }

    public function testGetStockLayoutById(): void
    {
        $stockLayoutId = 1;
        $stockLayoutEntity = new StockLayoutEntity();

        $this->mockObject
            ->expects(self::once())
            ->method('getStockLayoutById')
            ->with($stockLayoutId)
            ->willReturn($stockLayoutEntity);

        $result = $this->stockLayoutService->getStockLayoutById($stockLayoutId);

        self::assertSame($stockLayoutEntity, $result);
    }

    public function testAddStockLayout(): void
    {
        $stockLayoutEntity = new StockLayoutEntity();
        $this->mockObject
            ->expects(self::once())
            ->method('addStockLayout')
            ->with($stockLayoutEntity);

        $this->stockLayoutService->addStockLayout($stockLayoutEntity);
    }

    public function testUpdateStockLayout(): void
    {
        $stockLayoutEntity = new StockLayoutEntity();
        $this->mockObject
            ->expects(self::once())
            ->method('updateStockLayout')
            ->with($stockLayoutEntity);

        $this->stockLayoutService->updateStockLayout($stockLayoutEntity);
    }

    public function testDeleteStockLayout(): void
    {
        $stockLayoutEntity = new StockLayoutEntity();
        $this->mockObject
            ->expects(self::once())
            ->method('deleteStockLayout')
            ->with($stockLayoutEntity);

        $this->stockLayoutService->deleteStockLayout($stockLayoutEntity);
    }
}
