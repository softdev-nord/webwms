$(document).ready(function() {
    $('#add_supplier_order_supplierOrderDate').datepicker(
        {
            todayHighlight: true,
        }
    );

    $('#add_customer_order_customerOrderDate').datepicker(
        {
            todayHighlight: true,
        }
    );
});
