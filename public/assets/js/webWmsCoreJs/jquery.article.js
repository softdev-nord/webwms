;(function($){
    // Artikel Tabelle
    const artTable = $('#artTable').DataTable({
        lengthChange: false,

        ajax: {
            url: '/article_ajax',
            dataSrc: ''
        },
        // Seitenlänge max. 10 Einträge
        pageLength: 10,
        language: {
            url: './resources/dataTable.German.json'
        },
        // Initialisierung der DataTables Select-Erweiterung
        select: {
            style: 'single'
        },
        columns: [
            { data: 'article_nr' },
            { data: 'article_name' },
            { data: 'article_category' },
            { data: 'article_weight' },
            { data: 'article_ean' },
            { data: 'article_unit' },
            { data: 'article_depth' },
            { data: 'article_width' },
            { data: 'article_height' },
            { data: 'in_stock' },
            { data: 'incoming_stock' },
            { data: 'reserved_stock' },
            {
                data: null,
                render: function(data, type, row) {
                    if (row.updated_at != null) {
                        return row.updated_at;
                    } else {
                        return row.created_at;
                    }
                },
            }
        ],
        columnDefs: [
            {
                className: 'text-center',
                targets: '_all'
            },
            {
                render: $.fn.dataTable.render.number('.'),
                'targets': [9,10,11],
            },
        ],
        dom: 'Bfrtip',
        buttons: {
            buttons: [
                {
                    extend:    'copyHtml5',
                    text:      'Kopieren',
                    title:     'Export',
                    titleAttr: 'Copy',
                    className: 'btn btn-primary btn-xm btn3d'
                },
                {
                    extend:    'csvHtml5',
                    text:      'CSV',
                    title:     'Export',
                    titleAttr: 'CSV',
                    className: 'btn btn-primary btn-xm btn3d'
                },
                {
                    text:       'JSON',
                    title:      'Export',
                    action: function (e, dt, button, config) {
                        DataTable.fileSave(
                            new Blob(
                                [
                                    JSON.stringify(convertArticleOverviewToJson())
                                ]
                            ),
                            'export_article_overview.json'
                        );
                    },
                    className: 'btn btn-primary btn-xm btn3d'
                },
                {
                    extend:    'pdfHtml5',
                    text:      'PDF',
                    title:     'Export',
                    titleAttr: 'PDF',
                    className: 'btn btn-primary btn-xm btn3d'
                },
                {
                    extend: 'print',
                    text: 'Drucken',
                    autoPrint: false,
                    className: 'btn btn-primary btn-xm btn3d'
                },
                {
                    text: 'Artikel anlegen',
                    className: 'btn-primary btn-xm btn3d float-start',
                    action: function(e, dt, node, config) {
                        addArticle();
                    }
                }
            ],
            dom: {
                button: {
                    className: 'btn'
                },
                buttonLiner: {
                    tag: null
                }
            }
        }
    });

    $.contextMenu({
        selector: 'tr',
        trigger: 'right',
        callback: function(key, options, event) {
            const row = artTable.row(options.$trigger),
                articleId = row.data().article_id,
                articleNr = row.data().article_nr;

            switch (key) {
                case 'edit' :
                    editArticle(articleId);
                    break;
                case 'delete' :
                    deleteArticle(articleId);
                    break;
                case 'show' :
                    getStockOccupancyByArticleNr(articleNr);
                    break;
                default :
                    break;
            }
        },
        items: {
            edit: {
                name: 'Bearbeiten',
                icon: 'edit'
            },
            delete: {
                name: 'Löschen',
                icon: 'delete'
            },
            show: {
                name: 'Artikel Lagerbelegungen',
                icon: 'paste'
            },
        }
    });

    // Artikel als Json exportieren
    function convertArticleOverviewToJson() {
        const objects = [];
        const data = artTable.rows().data();

        for (let i = 0; i < data.length; i++) {
            objects.push(data[i]);
        }

        return objects;
    }

    function getStockOccupancyByArticleNr(article_nr) {
        const url = '/stock_occupancy_ajax_article/' + article_nr;
        const content = '<div class="modal-body"></div>';
        const headerText = 'Lagerbelegungen für Artikel-Nr. ' + article_nr;
        $("#modalCenter .modal-dialog").css('max-width', '90%');

        $('#modalCenter .modal-title').text(headerText);
        $("#modal-content-ajax").html(content);
        $('#modalCenter').modal('show');

        $.ajax({
            method: 'GET',
            url: url,
            dataType: 'html',
            success: function (data) {
                $('#modal-content-ajax').html(data);
            }
        });
    }

    $(function(){
        // Ändern der Standardbreite des Modals
        $('#modalCenter .modal-dialog').css('max-width', '98%');
    });

    $.ajaxSetup({
        cache: false
    });

    // Modal für Artikel anlegen
    function addArticle() {
        const url = '/artikel_anlegen',
            $form = $('form#article-form-new'),
            title = 'Artikel anlegen';

        getContentForModal(url, title, $form);
    }

    // Modal für Artikel bearbeiten
    function editArticle(id) {
        const url = '/artikel_bearbeiten/articleId/' + id,
            $form = $('form#article-form-edit'),
            title = 'Artikel bearbeiten';

        getContentForModal(url, title, $form);
    }

    // Modal für Artikel löschen
    function deleteArticle(articleId) {
        const url = '/artikel_löschen/articleId/' + articleId,
            $form = $('form#article-modal-delete-ask'),
            title = 'Artikel löschen';

        $('#modalCenter .modal-dialog').css('max-width', '30%');

        getContentForModal(url, title, $form);
    }

    // Neuen Artikel speichern
    $(document).on('click', 'button#add_article_save', function(event) {
        const $form = $('form#article-form-new'),
            url = '/artikel_anlegen',
            errorMessage = 'Artikel konnte nicht gespeichert werden',
            successMessage = 'Artikel erfolgreich gespeichert';
        event.preventDefault();

        _doRequest('POST', url, $form, errorMessage, successMessage, artTable, false);
    });

    // Geänderten Artikel speichern
    $(document).on('click', 'button#edit_article_save', function(event) {
        const articleId = $('#edit_article_articleId').val(),
            $form = $('form#article-form-edit'),
            url = '/artikel_bearbeiten/articleId/' + articleId,
            errorMessage = 'Artikel konnte nicht gespeichert werden',
            successMessage = 'Artikel erfolgreich gespeichert';
        event.preventDefault();

        _doRequest('POST', url, $form, errorMessage, successMessage, artTable, false);
    });

    // Artikel löschen
    $(document).on('click', 'button#delete_article_delete', function(event) {
        const articleId = $('#delete_article_articleId').val(),
            $form = $('form#article-modal-delete-ask'),
            url = '/artikel_löschen/articleId/' + articleId,
            errorMessage = 'Artikel konnte nicht gelöscht werden',
            successMessage = 'Artikel erfolgreich gelöscht';
        event.preventDefault();

        _doRequest('POST', url, $form, errorMessage, successMessage, artTable, false);
    });

    $(document).on('click', '.abort', function() {
        $('#modalCenter').modal('hide');
    });

})(jQuery);
