<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240405092756 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE INDEX stock_location_id_idx ON stock_occupancy (stock_location_id)');
        $this->addSql('CREATE INDEX article_id_idx ON stock_occupancy (article_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX stock_location_id_idx ON stock_occupancy');
        $this->addSql('DROP INDEX article_id_idx ON stock_occupancy');
    }
}
