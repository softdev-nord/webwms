# This file is the entry point to configure your own services.
# Files in the packages/ subdirectory configure your dependencies.

# Put parameters here that don't need to change on each machine where the app is deployed
# https://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: '%env(LOCALE)%'
    fromMail:  '%env(FROM_MAIL)%'
    fromName: '%env(FROM_NAME)%'
    returnPath: '%env(RETURN_PATH)%'
    mailHost:  '%env(MAIL_HOST)%'
    webHost: '%env(WEB_HOST)%'
    publicUploadDirectory: 'export'
    uploadDirectory: '%kernel.project_dir%/public/%publicUploadDirectory%'
    version: '3.3.0'
    mailCopy: '%env(default:default_mailcopy:MAIL_COPY)%'
    default_mailcopy: 'true'

services:
    # default configuration for services in *this* file
    _defaults:
        autowire: true      # Automatically injects dependencies in your services.
        autoconfigure: true # Automatically registers your services as commands, event subscribers, etc.
        public: true         // <---- This is the new additional config.

        bind:
            string $appName: '%env(APP_NAME)%'
            string $appVersion: '%env(APP_VERSION)%'
            string $appVersionNumber: '%env(APP_VERSION_NUMBER)%'
            string $appCopyright: '%env(APP_COPYRIGHT)%'
            string $appLizenz: '%env(APP_LIZENZ)%'
            # Resolve issue:  Cannot autowire service : argument of method "__construct()" is type-hinted "string", you should configure its value explicitly.
            string $publicDirectory: '%publicUploadDirectory%'
            string $targetDirectory: '%uploadDirectory%'
            string $fromMail: '%fromMail%'
            string $fromName: '%fromName%'
            string $returnPath: '%returnPath%'
            string $mailCopy: '%mailCopy%'
            string $webHost: '%webHost%'
            string $bundleDir: '/var/www/html/module'
            string $projectDir: '%kernel.project_dir%'

    Symfony\Component\DependencyInjection\ContainerInterface: '@service_container'

    # makes classes in src/ available to be used as services
    # this creates a service per class whose id is the fully-qualified class name
    WebWMS\:
        resource: '../src/'
        exclude:
            - '../src/DependencyInjection/'
            - '../src/Kernel.php'
            - '../src/Tests/'

    # controllers are imported separately to make sure services can be injected
    # as action arguments even if you don't extend any base controller class
    WebWMS\Controller\:
        resource: '../src/Controller/'
        tags: ['controller.service_arguments']

    # add more service definitions when explicit configuration is needed
    # please note that last definitions always *replace* previous ones

    web_wms.form.type.task:
        class: WebWMS\Form\Article\EditArticleType
        public: true
        arguments: [ "@security.authorization_checker" ]
        tags:
            - { name: form.type }
    
    web_wms.service.transport_request_service:
        class: WebWMS\Service\TransportRequest\TransportRequestService
    
    web_wms.service.data_handlers.configuration.configuration_data_handler:
        class: WebWMS\Service\DataHandlers\Configuration\ConfigurationDataHandler

#    web_wms.controller.stock_transactions:
#        class: WebWMS\Controller\StockTransactionController
#        public: true
#        autowire: true
#        tags: ['controller.service_arguments']
#        arguments:
#            $bookingMethodService: '@web_wms.service.booking_methode.booking_methode_service'
#            $transportRequestService: '@web_wms.service.transport_request_service'

    web_wms.service.file_uploader:
        class: WebWMS\Service\FileUploader
        autowire: true
        arguments:
            $publicDirectory: '%publicUploadDirectory%'
            $targetDirectory: '%uploadDirectory%'

    web_wms.service.configuration.configuration_service:
        class: WebWMS\Service\Configuration\ConfigurationService
        arguments:
            $configurationDataHandler: '@web_wms.service.data_handlers.configuration.configuration_data_handler'

    web_wms.service.logging_service:
        class: WebWMS\Service\LoggingService

    web_wms.controller.dashboard_controller:
        class: WebWMS\Controller\DashboardController
        arguments:
            $requirementsService: 'WebWMS\Service\RequirementsService'
            $twigEnvironment: 'Twig\Environment'
            $chartBuilder: 'Symfony\UX\Chartjs\Builder\ChartBuilderInterface'
            $transportHistoryRepository: 'WebWMS\Repository\TransportHistoryRepository'
            $stockRotationService: 'WebWMS\Service\Stock\StockRotationService'
            $transportRequestService: 'WebWMS\Service\TransportRequestService'
            $stockLocationService: 'WebWMS\Service\Stock\StockLocationService'

    twig.extension.debug:
        class: Twig\Extension\DebugExtension
        tags: [ { name: 'twig.extension' } ]

    Symfony\Component\HttpFoundation\Request:
        autowire: true
